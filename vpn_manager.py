#! /usr/bin/env python3

import argparse
import libtmux
import os
import sys
import vpn_root


SESSION_NAME = "OpenVPN"
CONTROL_NAME = "control"


def _lookup_session(session_name=None):
    if not session_name:
        session_name = SESSION_NAME

    server = libtmux.Server()
    session = None
    try:
        session = server.find_where({"session_name": session_name})
    except libtmux.exc.LibTmuxException:
        pass
    return session


def _create_session(session_name=None, window_name=None):
    if not session_name:
        session_name = SESSION_NAME
    if not window_name:
        window_name = CONTROL_NAME

    server = libtmux.Server()
    command = f"{sys.argv[0]} {session_name}"
    session = server.new_session(
        session_name=session_name,
        window_name=window_name,
        kill_session=True,
        attach=False,
        start_directory=os.getcwd(),
        window_command=command,
    )
    return session


def _attach_session(session_name):
    server = libtmux.Server()
    server.attach_session(session_name)


def create_server():
    session = _lookup_session()
    if not session:
        print("Creating server")
        session = _create_session()
    if not session:
        raise Exception("Failed to create openvpn server")
    print(session)
    return session


def kill_server():
    print("Killing server")
    session = _lookup_session()
    if session:
        session.kill_session()
    return session


if __name__ == "__main__":  # pragma: no cover
    parser = argparse.ArgumentParser(description="Complete management of OpenVPN systems")

    parser.add_argument("--kill-server", action="store_true", help="Kills the OpenVPN manager")

    parser.add_argument("session", type=str, nargs="?", help=argparse.SUPPRESS)

    args = parser.parse_args()

    if args.kill_server:
        kill_server
    elif args.session:
        server = libtmux.Server()
        session = server.find_where({"session_name": args.session})
        while True:
            try:
                root = vpn_root.VPN_Root(server, session, CONTROL_NAME)
                root.cmdloop()
            except TimeoutError:
                pass
    else:
        create_server()

        # Only attempt to attach if not in an interactive session
        if sys.__stdin__.isatty():
            _attach_session(SESSION_NAME)
