import libtmux
import signal
from cmd import Cmd


def handle_timeout(signum, frame):
    raise TimeoutError()


class TimedCmd(Cmd):
    def __init__(self):
        signal.alarm(5)
        super().__init__()

    def __del__(self):
        signal.alarm(0)

    def precmd(self, line):
        signal.alarm(5)
        return line


class VPN_Enable(TimedCmd):
    def __init__(self, session):
        self.session = session
        self.prompt = "# "
        super().__init__()

    def do_hello(self, line):
        print("Hi!")

    def do_exit(self, line):
        """Leaves privileged mode."""
        print()
        return True

    def do_EOF(self, line):
        """Leaves privileged mode."""
        return self.do_exit(line)


class VPN_Root(Cmd):
    def __init__(self, server, session, control_name):
        try:
            import readline

            readline.clear_history()
        except ImportError:
            pass

        self.server = libtmux.Server()
        self.session = session
        self.control_name = control_name
        self.prompt = "> "

        self.intro = """
VPN_Manager
Type 'help' for help.
"""
        signal.signal(signal.SIGALRM, handle_timeout)
        super().__init__()

    def emptyline(self):
        pass

    def do_exit(self, line):
        """Detach from the current session."""
        print()
        self.server.cmd("detach")

    def do_EOF(self, line):
        """Detach from the current session."""
        return self.do_exit(line)

    def do_FORCE_EXIT(self, line):
        """Destroys the entire VPN Manager session."""
        self.session.kill_session()
        return True

    def do_enable(self, line):
        VPN_Enable(self.session).cmdloop()
