import os
import pytest
import subprocess
import tempfile
import vpn_key_manager
from dataclasses import dataclass
from unittest.mock import patch


@dataclass
class FakeOpenSSLRun:
    stdout: str


FORCE_SMALL_KEYS = {"OPENVPN": {"DH_KEY_SIZE": 1024, "RSA_KEY_SIZE": 1024}}


def test_dir_exists():
    with tempfile.TemporaryDirectory() as d:
        with pytest.raises(FileNotFoundError):
            vpn_key_manager.VPN_Key_Manager(d)


def test_simple_setup():
    with tempfile.TemporaryDirectory() as d:
        os.rmdir(d)
        vpn_key_manager.VPN_Key_Manager(d)

        # Confirm the directory can still be used
        vpn_key_manager.VPN_Key_Manager(d)

        # Confirm that init can be called
        vpn_key_manager.VPN_Key_Manager(d).init_ca(**FORCE_SMALL_KEYS)


def test_simple_init():
    with tempfile.TemporaryDirectory() as d:
        os.rmdir(d)
        vpn = vpn_key_manager.VPN_Key_Manager(d, verbose=2)

        assert vpn.is_initialised() is False
        with pytest.raises(vpn_key_manager.NotInitialisedException):
            vpn.assert_initialised()

        vpn.init_ca(**FORCE_SMALL_KEYS)

        assert vpn.is_initialised()
        vpn.assert_initialised()

        with pytest.raises(vpn_key_manager.AlreadyInitialisedException):
            vpn.init_ca(**FORCE_SMALL_KEYS)

        with pytest.raises(subprocess.CalledProcessError):
            vpn.get_server_serial("does_not_exist")

        vpn.create_server("server", OPENVPN={"COMMON_OPTS": {"port": 1234}})
        assert 1 == vpn.get_server_serial("server")

        vpn.create_client("client")
        assert 2 == vpn.get_client_serial("client")

        with pytest.raises(FileExistsError):
            vpn.create_client("client")

        vpn.revoke_server("server")
        vpn.revoke_client("client")

        # Revoked certs cannot have their serial numbers retrieved
        with pytest.raises(subprocess.CalledProcessError):
            vpn.get_server_serial("server")

        vpn.list_crl()


def test_openssl_version():
    with tempfile.TemporaryDirectory() as d:
        with patch.object(subprocess, "run", return_value=FakeOpenSSLRun(b"0")):
            with pytest.raises(vpn_key_manager.UnsupportedVersionException):
                vpn_key_manager.VPN_Key_Manager(d)


def test_unexpected_openssl_serial():
    with tempfile.TemporaryDirectory() as d:
        os.rmdir(d)
        vpn = vpn_key_manager.VPN_Key_Manager(d)
        vpn.init_ca(**FORCE_SMALL_KEYS)
        vpn.create_server("server")
        with patch.object(subprocess, "run", return_value=FakeOpenSSLRun(b"0")):
            with pytest.raises(RuntimeError):
                vpn.get_server_serial("server")


def test_revoke_failure():
    with tempfile.TemporaryDirectory() as d:
        os.rmdir(d)
        vpn = vpn_key_manager.VPN_Key_Manager(d)

        vpn.init_ca(**FORCE_SMALL_KEYS)
        vpn.create_client("Name")

        with patch.object(os, "write", return_value=0):
            with pytest.raises(RuntimeError):
                vpn.revoke_client("Name")


def test_invalid_args():
    with tempfile.TemporaryDirectory() as d:
        os.rmdir(d)
        vpn = vpn_key_manager.VPN_Key_Manager(d, verbose=2)

        vpn.init_ca(**FORCE_SMALL_KEYS)

        INVALID_SERVER_OPTS = [
            {"SERVER_OPTS": {"local": "@"}},
            {"COMMON_OPTS": {"protocol": "invalid"}},
            {"COMMON_OPTS": {"port": -1}},
            {"COMMON_OPTS": {"dev": "invalid"}},
            {"COMMON_OPTS": {"cipher": "@"}},
        ]
        for opt in INVALID_SERVER_OPTS:
            with pytest.raises(ValueError):
                vpn.create_server("bad", OPENVPN=opt)

        INVALID_CLIENT_OPTS = [
            {"CLIENT_OPTS": {"remote": "@"}},
            {"COMMON_OPTS": {"protocol": "invalid"}},
            {"COMMON_OPTS": {"port": -1}},
            {"COMMON_OPTS": {"dev": "invalid"}},
            {"COMMON_OPTS": {"cipher": "@"}},
        ]
        for opt in INVALID_CLIENT_OPTS:
            with pytest.raises(ValueError):
                vpn.create_client("bad", OPENVPN=opt)

        INVALID_CONFIG_OPTS = [
            {"OPENVPN": {"RSA_KEY_SIZE": 5}},
            {"CLIENT": {"OU": "\n"}},
            {"CLIENT": {"EMAIL": "!"}},
            {"CLIENT": {"ORG": "\n"}},
            {"CLIENT": {"CITY": "\n"}},
            {"CLIENT": {"PROVINCE": "\n"}},
            {"CLIENT": {"COUNTRY": "\n"}},
        ]
        for opt in INVALID_CONFIG_OPTS:
            with pytest.raises(ValueError):
                vpn.create_client("bad", **opt)

        with pytest.raises(ValueError):
            vpn.create_client("*", **opt)
