#! /usr/bin/env python3

"""Manages a VPN server. Elements are strongly based on easy-rsa."""

import argparse
import json
import os
import string
import subprocess
import sys
import tempfile
from datetime import datetime
from pathlib import Path


class UnsupportedVersionException(Exception):
    pass


class NotInitialisedException(Exception):
    pass


class AlreadyInitialisedException(Exception):
    pass


OPENSSL = "openssl"
OPENVPN = "openvpn"

DEFAULT_CONFIG = """
{
    # Defines a default client config
    # These settings may be modified at will at any point when generating certificates
    "CLIENT":
    {
        # The default organizational unit
        "OU": "MyOrganizationalUnit",

        # The default email
        "EMAIL": "me@myhost.mydomain",

        # The default org
        "ORG": "Fort-Funston",

        # The default city
        "CITY": "SanFrancisco",

        # The default province
        "PROVINCE": "CA",

        # The default country
        "COUNTRY": "US"
    },

    # OpenVPN Settings
    # Changing these settings likely requires regenerating all certificates
    "OPENVPN":
    {
        # The Diffie-Hellman key size
        "DH_KEY_SIZE": 4096,

        # The RSA key size for the certificate
        "RSA_KEY_SIZE": 4096,

        # The max age for the certificate
        "RSA_KEY_EXPIRY": 3650,

        "COMMON_OPTS":
        {
            # The protocol to use. Options are udp or tcp
            "protocol": "udp",

            # The port to connect against
            "port": 1194,

            # The OpenVPN device to create. tap or tun. May have suffix. Eg tap-home
            "dev": "tap",

            # The cipher for the connection
            "cipher": "AES-256-CBC",

            # Keepalive pings for either side to detect a stale connection
            "keepalive": "10 120"
        },

        "CLIENT_OPTS":
        {
            # Required for client connections
            "client": null,

            # The address clients will use to connect to the server
            "remote": "127.0.0.1",

            # Identifies the remote endpoint as a server
            "remote-cert-tls": "server",

            # Used by clients
            "key-direction": 1
        },

        "SERVER_OPTS":
        {
            # The address OpenVPN will bind to
            "local": "0.0.0.0",

            # The address range to lease
            "server": "192.168.3.0 255.255.255.0",

            # Used to ensure endpoints get a consistent IP address
            "ifconfig-pool-persist": "ipp.txt",

            # Allows clients to talk to other clients
            "client-to-client": null,

            # Used by servers
            "key-direction": 0
        }
    }
}
"""


class VPN_Key_Manager:
    def __init__(self, path, verbose=0, **kwargs):
        """Initialise the VPN_Key_Manager.
        Creates the path if it does not already exist.
        A config file will be placed in it if created.
        The config file may be modified to adjust how the manager will create clients."""
        # Validate the openssl version
        p = subprocess.run([OPENSSL, "version"], check=True, stdout=subprocess.PIPE)
        version = p.stdout.decode("utf-8").split("\n")[0]

        if version.startswith("OpenSSL 1.1") is False:
            raise UnsupportedVersionException(f"Unsupported openssl version: {version}. Test it and update the script.")

        self.key_dir = path
        self.config_file = f"{path}/config"
        self.verbose = verbose
        # Define internal file state
        self.files = {
            "DH_FILE": f"{path}/dh.pem",
            "STATIC_SECRET": f"{path}/static.key",
            "CA_KEY": f"{path}/ca.key",
            "CA_CRT": f"{path}/ca.crt",
            "INDEX": f"{path}/index.txt",
            "INDEX_ATTR": f"{path}/index.txt.attr",
            "SERIAL": f"{path}/serial",
            "CRL": f"{path}/crl.pem",
        }

        if os.path.isdir(self.key_dir) is False:
            # No dir means perform full init
            os.makedirs(self.key_dir)
            # Write out default config
            with open(self.config_file, "w") as f:
                f.write(DEFAULT_CONFIG)

        # Load the global defaults
        self._load_defaults(**kwargs)

        # Always set directory perms
        os.chmod(self.key_dir, 0o700)

    def is_initialised(self):
        # Confirm the required files are now present
        for key in self.files:
            if os.path.isfile(self.files[key]) is False:
                return False
        return True

    def assert_initialised(self):
        # Confirm the required files are now present
        for key in self.files:
            if os.path.isfile(self.files[key]) is False:
                raise NotInitialisedException(f"Path {self.key_dir} not initialised. Missing {key}.")

    def init_ca(self, ca_name=None, **kwargs):
        if ca_name is None:
            ca_name = "CA"

        # Load the global defaults
        # Loaded in case the config file changed after the object was created
        self._load_defaults(**kwargs)

        # Before init, confirm that no files are present
        for key in self.files:
            if os.path.isfile(self.files[key]) is True:
                raise AlreadyInitialisedException(
                    f"Path {self.key_dir} already initialised?. {key} already present. Not proceeding"
                )

        self._initialise_key_dir()
        self._initialise_ca(ca_name)
        self._initialise_dh()
        self._initialise_static_secret()
        self.generate_crl()
        self.assert_initialised()

        if self.verbose > 0:
            print("Initialised")

    def generate_crl(self, **kwargs):
        # Load the global defaults
        # Loaded in case the config file changed after the object was created
        self._load_defaults(**kwargs)
        Path(self.files["CRL"]).touch()
        self.assert_initialised()

        with tempfile.NamedTemporaryFile("w") as config_file:
            self._generate_openssl_config(config_file, "CRL")

            stdout = subprocess.DEVNULL
            stderr = subprocess.DEVNULL
            if self.verbose > 1:
                stdout = None
                stderr = None
            subprocess.run(
                [OPENSSL, "ca", "-gencrl", "-out", self.files["CRL"], "-config", config_file.name],
                check=True,
                stdout=stdout,
                stderr=stderr,
            )

    def get_server_serial(self, name):
        return self._get_serial(name, True)

    def get_client_serial(self, name):
        return self._get_serial(name, False)

    def list_crl(self, **kwargs):
        # Load the global defaults
        # Loaded in case the config file changed after the object was created
        self._load_defaults(**kwargs)
        self.assert_initialised()

        subprocess.run([OPENSSL, "crl", "-text", "-noout", "-in", self.files["CRL"]], check=True)

    def create_server(self, name, **kwargs):
        # Load the global defaults
        # Loaded in case the config file changed after the object was created
        self._load_defaults(**kwargs)
        self.assert_initialised()

        # Validate args
        address = self.defaults["OPENVPN"]["SERVER_OPTS"]["local"]
        address_valid_chars = string.ascii_lowercase + string.ascii_uppercase + string.digits + "._"
        if self._validate_string_subset(address, address_valid_chars) is False:
            raise ValueError(f"Invalid address: {address}")

        protocol = self.defaults["OPENVPN"]["COMMON_OPTS"]["protocol"]
        if protocol not in ["udp", "tcp"]:
            raise ValueError(f"Invalid protocol: {protocol}")

        port = self.defaults["OPENVPN"]["COMMON_OPTS"]["port"]
        if port <= 0:
            raise ValueError(f"Invalid port: {port}")

        dev = self.defaults["OPENVPN"]["COMMON_OPTS"]["dev"]
        if dev.startswith("tun") is False and dev.startswith("tap") is False:
            raise ValueError(f"Invalid device: {dev}")

        cipher = self.defaults["OPENVPN"]["COMMON_OPTS"]["cipher"]
        cipher_valid_chars = string.ascii_lowercase + string.ascii_uppercase + string.digits + "-"
        if self._validate_string_subset(cipher, cipher_valid_chars) is False:
            raise ValueError(f"Invalid cipher: {cipher}")

        key_file, crt_file = self._create_key(name, server=True)
        with open(self.files["CA_CRT"], "r") as ca_cert:
            ca_cert_data = ca_cert.read()
        with open(self.files["DH_FILE"], "r") as dh_key:
            dh_key_data = dh_key.read()
        with open(self.files["STATIC_SECRET"], "r") as static_secret:
            static_secret_data = static_secret.read()
        with open(crt_file, "r") as cert:
            cert_data = cert.read()
        with open(key_file, "r") as key:
            key_data = key.read()

        ovpn_file = f"{self.key_dir}/server_{name}.ovpn"
        with open(ovpn_file, "w") as ovpn:
            opts = self.defaults["OPENVPN"]["COMMON_OPTS"]
            opts.update(self.defaults["OPENVPN"]["SERVER_OPTS"])
            for opt in opts:
                if opts[opt] is None:
                    ovpn.write(f"{opt}\n")
                else:
                    ovpn.write(f"{opt} {opts[opt]}\n")

            ovpn.write(
                f"""
<ca>
{ca_cert_data}
</ca>

<cert>
{cert_data}
</cert>

<key>
{key_data}
</key>

<dh>
{dh_key_data}
</dh>

<tls-auth>
{static_secret_data}
</tls-auth>
"""
            )

        if self.verbose > 0:
            print(f"Generated server: {name}")

        return ovpn_file

    def create_client(self, name, **kwargs):
        # Load the global defaults
        # Loaded in case the config file changed after the object was created
        self._load_defaults(**kwargs)
        self.assert_initialised()

        # Validate args
        address = self.defaults["OPENVPN"]["CLIENT_OPTS"]["remote"]
        address_valid_chars = string.ascii_lowercase + string.ascii_uppercase + string.digits + "._"
        if self._validate_string_subset(address, address_valid_chars) is False:
            raise ValueError(f"Invalid address: {address}")

        protocol = self.defaults["OPENVPN"]["COMMON_OPTS"]["protocol"]
        if protocol not in ["udp", "tcp"]:
            raise ValueError(f"Invalid protocol: {protocol}")

        port = self.defaults["OPENVPN"]["COMMON_OPTS"]["port"]
        if port <= 0:
            raise ValueError(f"Invalid port: {port}")

        dev = self.defaults["OPENVPN"]["COMMON_OPTS"]["dev"]
        if dev.startswith("tun") is False and dev.startswith("tap") is False:
            raise ValueError(f"Invalid device: {dev}")

        cipher = self.defaults["OPENVPN"]["COMMON_OPTS"]["cipher"]
        cipher_valid_chars = string.ascii_lowercase + string.ascii_uppercase + string.digits + "-"
        if self._validate_string_subset(cipher, cipher_valid_chars) is False:
            raise ValueError(f"Invalid cipher: {cipher}")

        key_file, crt_file = self._create_key(name, server=False)
        with open(self.files["CA_CRT"], "r") as ca_cert:
            ca_cert_data = ca_cert.read()
        with open(self.files["STATIC_SECRET"], "r") as static_secret:
            static_secret_data = static_secret.read()
        with open(crt_file, "r") as cert:
            cert_data = cert.read()
        with open(key_file, "r") as key:
            key_data = key.read()

        ovpn_file = f"{self.key_dir}/client_{name}.ovpn"
        with open(ovpn_file, "w") as ovpn:
            opts = self.defaults["OPENVPN"]["COMMON_OPTS"]
            opts.update(self.defaults["OPENVPN"]["CLIENT_OPTS"])
            for opt in opts:
                if opts[opt] is None:
                    ovpn.write(f"{opt}\n")
                else:
                    ovpn.write(f"{opt} {opts[opt]}\n")

            ovpn.write(
                f"""
<ca>
{ca_cert_data}
</ca>

<cert>
{cert_data}
</cert>

<key>
{key_data}
</key>

<tls-auth>
{static_secret_data}
</tls-auth>
"""
            )

        if self.verbose > 0:
            print(f"Generated client: {name}")

        return ovpn_file

    def revoke_client(self, name, **kwargs):
        # Load the global defaults
        # Loaded in case the config file changed after the object was created
        self._load_defaults(**kwargs)
        self.assert_initialised()

        return self._revoke_key(name, server=False)

    def revoke_server(self, name, **kwargs):
        # Load the global defaults
        # Loaded in case the config file changed after the object was created
        self._load_defaults(**kwargs)
        self.assert_initialised()

        return self._revoke_key(name, server=True)

    def _get_serial(self, name, server):
        crt_file = f"{self.key_dir}/server_{name}.crt"
        if server is False:
            crt_file = f"{self.key_dir}/client_{name}.crt"
        p = subprocess.run([OPENSSL, "x509", "-serial", "-noout", "-in", crt_file], check=True, stdout=subprocess.PIPE)
        serial = p.stdout.decode("utf-8").split("\n")[0]

        if serial.startswith("serial=") is False:
            raise RuntimeError(f"Unexpected output from openssl: {serial}")

        return int(serial.split("=", 1)[1])

    def _deep_merge(self, source, destination):
        """
        run me with nosetests --with-doctest file.py

        >>> a = { 'first' : { 'all_rows' : { 'pass' : 'dog', 'number' : '1' } } }
        >>> b = { 'first' : { 'all_rows' : { 'fail' : 'cat', 'number' : '5' } } }
        >>> _deep_merge(b, a) == { 'first' : { 'all_rows' : { 'pass' : 'dog', 'fail' : 'cat', 'number' : '5' } } }
        True
        """
        for key, value in source.items():
            if isinstance(value, dict):
                # get node or create one
                node = destination.setdefault(key, {})
                self._deep_merge(value, node)
            else:
                destination[key] = value

        return destination

    def _load_defaults(self, **kwargs):
        with open(self.config_file) as f:
            data = "".join(filter(lambda x: x.lstrip().startswith("#") is False, f.read().split("\n"),))
            self.defaults = json.loads(data)
        # Update defaults with provided defaults
        self._deep_merge(kwargs, self.defaults)

    def _initialise_key_dir(self):
        # Touch index.txt
        open(self.files["INDEX"], "w").close()

        with open(self.files["INDEX_ATTR"], "w") as f:
            f.write("unique_subject = yes\n")

        with open(self.files["SERIAL"], "w") as f:
            f.write("01\n")

    def _initialise_ca(self, ca_name):
        with tempfile.NamedTemporaryFile("w") as config_file:
            self._generate_openssl_config(config_file, ca_name)

            stdout = subprocess.DEVNULL
            stderr = subprocess.DEVNULL
            if self.verbose > 1:
                stdout = None
                stderr = None
            subprocess.run(
                [
                    OPENSSL,
                    "req",
                    "-batch",
                    "-days",
                    f"{self.defaults['OPENVPN']['RSA_KEY_EXPIRY']}",
                    "-nodes",
                    "-new",
                    "-newkey",
                    f"rsa:{self.defaults['OPENVPN']['RSA_KEY_SIZE']}",
                    "-x509",
                    "-keyout",
                    self.files["CA_KEY"],
                    "-out",
                    self.files["CA_CRT"],
                    "-config",
                    config_file.name,
                ],
                check=True,
                stdout=stdout,
                stderr=stderr,
            )
            os.chmod(self.files["CA_KEY"], 0o600)

    def _validate_string_subset(self, string, character_subset):
        return set(string) <= set(character_subset)

    def _generate_openssl_config(self, config_file, name):
        # Validate args
        if self.defaults["OPENVPN"]["RSA_KEY_SIZE"] not in [1024, 2048, 4096]:
            raise ValueError(f"Invalid key size: {self.defaults['OPENVPN']['RSA_KEY_SIZE']}")

        key_name_valid_chars = string.ascii_lowercase + string.ascii_uppercase + string.digits + "._"
        if self._validate_string_subset(name, key_name_valid_chars) is False:
            raise ValueError(f"Invalid name: {name}")

        ou = self.defaults["CLIENT"]["OU"]
        key_ou_valid_chars = string.ascii_lowercase + string.ascii_uppercase + string.digits + string.punctuation + " "
        if self._validate_string_subset(ou, key_ou_valid_chars) is False:
            raise ValueError(f"Invalid OU: {os}")

        email = self.defaults["CLIENT"]["EMAIL"]
        key_email_valid_chars = string.ascii_lowercase + string.ascii_uppercase + string.digits + "@."
        if self._validate_string_subset(email, key_email_valid_chars) is False:
            raise ValueError(f"Invalid email: {email}")

        org = self.defaults["CLIENT"]["ORG"]
        key_org_valid_chars = string.ascii_lowercase + string.ascii_uppercase + string.digits + string.punctuation + " "
        if self._validate_string_subset(org, key_org_valid_chars) is False:
            raise ValueError(f"Invalid organisation: {org}")

        city = self.defaults["CLIENT"]["CITY"]
        key_city_valid_chars = (
            string.ascii_lowercase + string.ascii_uppercase + string.digits + string.punctuation + " "
        )
        if self._validate_string_subset(city, key_city_valid_chars) is False:
            raise ValueError(f"Invalid city: {city}")

        province = self.defaults["CLIENT"]["PROVINCE"]
        key_province_valid_chars = (
            string.ascii_lowercase + string.ascii_uppercase + string.digits + string.punctuation + " "
        )
        if self._validate_string_subset(province, key_province_valid_chars) is False:
            raise ValueError(f"Invalid province: {province}")

        country = self.defaults["CLIENT"]["COUNTRY"]
        key_country_valid_chars = (
            string.ascii_lowercase + string.ascii_uppercase + string.digits + string.punctuation + " "
        )
        if self._validate_string_subset(country, key_country_valid_chars) is False:
            raise ValueError(f"Invalid country: {country}")

        key_altnames = f"DNS:{name}"

        config_file.write(
            f"""
# Strongly based off easy-rsa
 [ ca ]
 default_ca              = CA_default               # The default ca section

####################################################################
 [ CA_default ]
 dir                     = {self.key_dir}           # Where everything is kept
 certs                   = $dir                     # Where the issued certs are kept
 crl_dir                 = $dir                     # Where the issued crl are kept
 database                = {self.files["INDEX"]}    # database index file.
 new_certs_dir           = $dir                     # default place for new certs.
 unique_subject          = yes

 certificate             = {self.files["CA_CRT"]}   # The CA certificate
 serial                  = {self.files["SERIAL"]}   # The current serial number
 crl                     = {self.files["CRL"]}      # The current CRL
 private_key             = {self.files["CA_KEY"]}   # The private key

 x509_extensions         = usr_cert                 # The extentions to add to the cert
 crl_extensions          = crl_ext

 default_days            = 3650                     # how long to certify for
 default_crl_days        = 30                       # how long before next CRL
 default_md              = sha256                   # use public key default MD
 preserve                = no                       # keep passed DN ordering

# A few difference way of specifying how similar the request should look
# For type CA, the listed attributes must be the same, and the optional
# and supplied fields are just that :-)
 policy                  = policy_anything

# For the CA policy
 [ policy_match ]
 countryName             = match
 stateOrProvinceName     = match
 organizationName        = match
 organizationalUnitName  = optional
 commonName              = supplied
 name                    = optional
 emailAddress            = optional

# For the 'anything' policy
# At this point in time, you must list all acceptable 'object'
# types.
 [ policy_anything ]
 countryName             = optional
 stateOrProvinceName     = optional
 localityName            = optional
 organizationName        = optional
 organizationalUnitName  = optional
 commonName              = supplied
 name                    = optional
 emailAddress            = optional

####################################################################
 [ req ]
 prompt                  = no
 default_bits            = {self.defaults["OPENVPN"]["RSA_KEY_SIZE"]}
 default_keyfile         = privkey.pem
 default_md              = sha256
 distinguished_name      = req_distinguished_name
 x509_extensions         = v3_ca                    # The extentions to add to the self signed cert

# This sets a mask for permitted string types. There are several options.
# default: PrintableString, T61String, BMPString.
# pkix     : PrintableString, BMPString (PKIX recommendation after 2004).
# utf8only: only UTF8Strings (PKIX recommendation after 2004).
# nombstr : PrintableString, T61String (no BMPStrings or UTF8Strings).
# MASK:XXXX a literal mask value.
 string_mask             = nombstr

 req_extensions          = v3_req                   # The extensions to add to a certificate request

 [ req_distinguished_name ]
 countryName             = {self.defaults["CLIENT"]["COUNTRY"]}
 stateOrProvinceName     = {self.defaults["CLIENT"]["PROVINCE"]}
 localityName            = {self.defaults["CLIENT"]["CITY"]}
 0.organizationName      = {self.defaults["CLIENT"]["ORG"]}

 name                    = {name}
 commonName              = {name}
 emailAddress            = {self.defaults["CLIENT"]["EMAIL"]}

 organizationalUnitName  = {self.defaults["CLIENT"]["OU"]}

 [ usr_cert ]

# These extensions are added when 'ca' signs a request.

# This goes against PKIX guidelines but some CAs do it and some software
# requires this to avoid interpreting an end user certificate as a CA.

 basicConstraints        = CA:FALSE

# PKIX recommendations harmless if included in all certificates.
 subjectKeyIdentifier    = hash
 authorityKeyIdentifier  = keyid,issuer:always
 extendedKeyUsage        = clientAuth
 keyUsage                = digitalSignature

 subjectAltName          = {key_altnames}

 [ server ]
 basicConstraints        = CA:FALSE
 nsCertType              = server
 nsComment               = "Automatically Generated Server Certificate"
 subjectKeyIdentifier    = hash
 authorityKeyIdentifier  = keyid,issuer:always
 extendedKeyUsage        = serverAuth
 keyUsage                = digitalSignature, keyEncipherment
 subjectAltName          = {key_altnames}

 [ v3_req ]
# Extensions to add to a certificate request
 basicConstraints        = CA:FALSE
 keyUsage                = nonRepudiation, digitalSignature, keyEncipherment

 [ v3_ca ]
# Extensions for a typical CA

# PKIX recommendation.
 subjectKeyIdentifier    = hash
 authorityKeyIdentifier  = keyid:always,issuer:always

 basicConstraints        = CA:true

 [ crl_ext ]
 authorityKeyIdentifier  = keyid:always,issuer:always
        """
        )
        config_file.flush()

    def _initialise_dh(self):
        if self.verbose > 0:
            print("Generating Diffie-Hellman key. May take a while.")

        stdout = subprocess.DEVNULL
        stderr = subprocess.DEVNULL
        if self.verbose > 1:
            stdout = None
            stderr = None
        subprocess.run(
            [OPENSSL, "dhparam", "-out", self.files["DH_FILE"], f"{self.defaults['OPENVPN']['DH_KEY_SIZE']}"],
            check=True,
            stdout=stdout,
            stderr=stderr,
        )

    def _initialise_static_secret(self):
        stdout = subprocess.DEVNULL
        stderr = subprocess.DEVNULL
        if self.verbose > 1:
            stdout = None
            stderr = None
        subprocess.run(
            [OPENVPN, "--genkey", "--secret", self.files["STATIC_SECRET"]], check=True, stdout=stdout, stderr=stderr
        )

    def _create_key(
        self, name, server,
    ):
        self.assert_initialised()

        if server:
            prefix = "server"
            extension = ["-extensions", "server"]
        else:
            prefix = "client"
            extension = []

        key_file = f"{self.key_dir}/{prefix}_{name}.key"
        csr_file = f"{self.key_dir}/{prefix}_{name}.csr"
        crt_file = f"{self.key_dir}/{prefix}_{name}.crt"

        if os.path.isfile(key_file) or os.path.isfile(csr_file) or os.path.isfile(crt_file):
            raise FileExistsError("Key name already exists")

        with tempfile.NamedTemporaryFile("w") as config_file:
            self._generate_openssl_config(config_file, name)

            stdout = subprocess.DEVNULL
            stderr = subprocess.DEVNULL
            if self.verbose > 1:
                stdout = None
                stderr = None

            subprocess.run(
                [
                    OPENSSL,
                    "req",
                    "-batch",
                    "-nodes",
                    "-new",
                    "-newkey",
                    f"rsa:{self.defaults['OPENVPN']['RSA_KEY_SIZE']}",
                    "-keyout",
                    key_file,
                    "-out",
                    csr_file,
                    "-config",
                    config_file.name,
                ]
                + extension,
                check=True,
                stdout=stdout,
                stderr=stderr,
            )

            subprocess.run(
                [
                    OPENSSL,
                    "ca",
                    "-batch",
                    "-days",
                    f"{self.defaults['OPENVPN']['RSA_KEY_EXPIRY']}",
                    "-out",
                    crt_file,
                    "-in",
                    csr_file,
                    "-config",
                    config_file.name,
                ]
                + extension,
                check=True,
                stdout=stdout,
                stderr=stderr,
            )

            os.unlink(csr_file)
            os.chmod(key_file, 0o600)
        return key_file, crt_file

    def _revoke_key(self, name, server):
        prefix = {True: "server", False: "client"}
        timestamp = datetime.utcnow().strftime("%Y%M%d.%H%M%S.%f")
        crt_file = f"{self.key_dir}/{prefix[server]}_{name}.crt"
        key_file = f"{self.key_dir}/{prefix[server]}_{name}.key"
        vpn_file = f"{self.key_dir}/{prefix[server]}_{name}.ovpn"
        crt_file_revoked = f"{crt_file}_revoked_{timestamp}"
        key_file_revoked = f"{key_file}_revoked_{timestamp}"
        vpn_file_revoked = f"{vpn_file}_revoked_{timestamp}"

        stdout = subprocess.DEVNULL
        stderr = subprocess.DEVNULL
        if self.verbose > 1:
            stdout = None
            stderr = None

        with tempfile.NamedTemporaryFile("w") as config_file:
            self._generate_openssl_config(config_file, "CRL")

            subprocess.run(
                [OPENSSL, "ca", "-revoke", crt_file, "-config", config_file.name],
                check=True,
                stdout=stdout,
                stderr=stderr,
            )

        self.generate_crl()

        # Validate the key has been revoked
        fd, temp_path = tempfile.mkstemp()
        with open(f"{self.files['CA_CRT']}", "rb") as file1:
            os.write(fd, file1.read())
        with open(f"{self.files['CRL']}", "rb") as file1:
            os.write(fd, file1.read())
        os.close(fd)

        p = subprocess.run(
            [OPENSSL, "verify", "-CAfile", temp_path, "-crl_check", crt_file], check=False, stdout=stdout, stderr=stderr
        )
        os.unlink(temp_path)

        if p.returncode != 2:
            raise RuntimeError("Failed to revoke cert")

        # Rename the files to indicate they've been revoked
        os.rename(crt_file, crt_file_revoked)
        os.rename(key_file, key_file_revoked)
        os.rename(vpn_file, vpn_file_revoked)

        if self.verbose > 0:
            print(f"Revoked {name}")


def main():  # pragma: no cover
    parser = argparse.ArgumentParser(description="Key management of OpenVPN systems")

    parser.add_argument("keydir", type=str, help="The directory to store keys inside")

    parser.add_argument("--init", action="store_true", default=False, help="Perform initialisation")
    parser.add_argument("-v", "--verbose", action="count", default=0, help="Increase verboseness")
    parser.add_argument("--generate-crl", action="store_true", default=False, help="Generate a new CRL")
    parser.add_argument("--list-crl", action="store_true", default=False, help="List the CRL")

    parser.add_argument("--server", default=[], action="append", metavar="SERVERNAME", help="Create server")
    parser.add_argument("--client", default=[], action="append", metavar="CLIENTNAME", help="Create client")

    parser.add_argument("--revoke-server", default=[], action="append", metavar="SERVERNAME", help="Revoke server")
    parser.add_argument("--revoke-client", default=[], action="append", metavar="CLIENTNAME", help="Revoke client")

    args = parser.parse_args()

    vpn = VPN_Key_Manager(args.keydir, verbose=args.verbose)

    if args.init:
        vpn.init_ca()

    for server in args.server:
        vpn.create_server(server)
    for client in args.client:
        vpn.create_client(client)
    for server in args.revoke_server:
        vpn.revoke_server(server)
    for client in args.revoke_client:
        vpn.revoke_client(client)

    if args.generate_crl:
        vpn.generate_crl()
        if vpn.verbose > 0:
            print("Generated CRL")

    if args.list_crl:
        vpn.list_crl()


if __name__ == "__main__":  # pragma: no cover
    sys.exit(main())
